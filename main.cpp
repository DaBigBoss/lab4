#include <boost/filesystem.hpp>
#include <iostream>
#include <map>
#include <string>

void Bypass(boost::filesystem::path s, std::map<std::string, std::string> &AccBroker, std::multimap<std::string, int> &AccDate)
{
    const boost::filesystem::path p{s};
    for(boost::filesystem::directory_entry& x:boost::filesystem::directory_iterator(p))
    {
        if(boost::filesystem::is_directory(x))  
            Bypass(x.path(),AccBroker,AccDate);
        else if((boost::filesystem::is_regular_file(x)) && (x.path().stem().string().substr(x.path().stem().string().length() - 3, 3) != "old") && (x.path().filename().string().substr(0, 7) == "balance"))
        {
            std::cout << x.path().parent_path().filename() << " " << x.path().filename() << '\n';
            AccBroker.insert(std::pair<std::string,std::string>(x.path().filename().string().substr(8, 8), x.path().parent_path().filename().string()));
            AccDate.insert(std::pair<std::string,int>(x.path().filename().string().substr(8, 8), atoi(x.path().filename().string().substr(17, 25).c_str())));
        }
    }
    return;
}
int main(int argc, char* argv[]) 
{
    boost::filesystem::path fspath;
    std::map<std::string, std::string> AccBroker;
    std::multimap<std::string, int> AccDate;
    if(argc<2)
    {
        fspath={"D:\\BMSTU\\AlgYa\\SEM3\\LAB4VS\\LAB4\\ftp\\"};
    }
    else
    {
        fspath={argv[1]};
    }
    Bypass(fspath,AccBroker,AccDate);
    std::vector<int> MAX;
    std::cout << std::endl;
    for(auto it=AccBroker.begin();it!=AccBroker.end();it++) 
    {
        int max=-1;
        for(auto at=AccDate.begin();at != AccDate.end();at++) 
        {
            if(it->first==at->first) 
            {
                if(at->second>max) 
                    max=at->second;
            }
        }
        std::cout << "broker: " << it->second << " account: " << it->first << " files: " << AccDate.count(it->first) << " last update: " << max << std::endl;
    }
    return 0;
}